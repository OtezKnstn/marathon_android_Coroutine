import kotlinx.coroutines.*
import java.text.SimpleDateFormat
import java.util.*

fun main() = runBlocking {
    val first = async { first_thread() }
    val second = async { second_thread() }
    val result = first.await() + second.await()
    printCurrentTime("Result: $result")
}

suspend fun first_thread(): Int {
	delay(1000L)
    printCurrentTime("World")
    return 1337
}

suspend fun second_thread(): Int {
    delay(2000L)
    printCurrentTime("Hello")
    return 1488
}

fun printCurrentTime(message: String) {
    val time = (SimpleDateFormat("hh:mm:ss")).format(Date())
    println("[$time] $message")
}
