import kotlinx.coroutines.*

fun main() {
    GlobalScope.launch {
        delay(1000L)
        println("World")
    }
    runBlocking {
        delay(2000L)
        println("Hello")
    }
}
